# onboarding notes

A place to learn how to work with gitlab using a mac and store some notes from onboarding

## TODO

Here is a markdown specific to things that are a "TODO"

TODO's will be now tracked as issues assigned to Jason
[TODO's](https://gitlab.com/jason.bowles_gl/onboard/-/issues)

### Category Boards
- [Status](https://gitlab.com/jason.bowles_gl/onboard/-/boards/4387589)
- [Type](https://gitlab.com/jason.bowles_gl/onboard/-/boards/4387598)
- [Priority](https://gitlab.com/jason.bowles_gl/onboard/-/boards/4387603)

## Questions

Here are outstanding questions that I need to ask someone that has been at Gitlab for longer than I have

[Questions](Questions.md)

## Completed

Every Question that was answered or TODO that was completed will be copied here..

[Completed](Completed.md)

## TIL Notes

Notes on things that I have learned on a day to day basis...  Slack history is only 90 days... so things that grab my attention will go here

[TIL Notes](til_notes.md)

## Job Related Links

List of links that I think I'll need to remember...

| Link Name | Link Note | Link URL |
|-----------|-----------|----------|
| New Todo  | This a link to a new issue | https://gitlab.com/jason.bowles_gl/onboard/-/issues/new |
| Okta  | SSO Links | https://gitlab.okta.com |
| gitlab unfiltered | Videos that are public and internal that are important | https://www.youtube.com/c/GitLabUnfiltered |
| Google Console | Login to google cloud | https://console.cloud.google.com |
| Gitlab Tools & Tips | Place where popular tools are documented | https://about.gitlab.com/handbook/tools-and-tips |
