# 6/3/2022

## Mac Learnings
- Found that after a screen shot you can mark up the image... not as smooth though
- can switch control and command... but sticking with command as control is only on the left side..
- Saw a vid on automatically compressing screenshots... hmm.. interesting

# 6/1/2022

## Coffee Chat with Jefferson Jones
Talk with Jefferson Jones and learned the following:

- [Gitlab Tools & Tips](https://about.gitlab.com/handbook/tools-and-tips)
- [Mac Docker via Colima](https://github.com/abiosoft/colima#getting-started)
- [Individ AWS & GCP Accounts](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#individual-aws-account-or-gcp-project)

