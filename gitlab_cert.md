# Gitlab Team Member Certification

- Must complete within 30 days of employment
- Per instructions keeping notes here

## Introduction
- [x] Started
- [x] Completed

Sun is the guide for learning Gitlab. Nicholas will also be helping...

![Learning Path](imgs/learning_path.png)

### Credentials
```
Array
(
    [account_invitation_id] => 733cbbcb-7711-4419-b645-fd1e0642f5b5
    [account_user_id] => 29286b00-8015-4bbe-9ef2-a9041e80d732
    [gitlab_url] => https://spt.gitlabtraining.cloud
    [username] => iu064oee
    [password] => iprgi0wv
    [created_at] => 2022-06-06 14:03:37
)
```

- GitLab URL: https://spt.gitlabtraining.cloud
- Username: iu064oee
- Password: iprgi0wv

### Markdown

- [Stlye Guide](https://about.gitlab.com/handbook/markdown-guide)
- [Markdown Shortcuts]()https://cloud.scorm.com/content/courses/L4NF4NVD5S/gitlab8570041/8/assets/pvFrs2RlqbUXA3ic_jDgDy6-p0tGrj7BQ-Markdown%20shortcuts.pdf
- [5 Min Intro](https://youtu.be/Ix416lAYRSg)

iu064oee-group/top-level-project#1



## Discovering Collaboration
- [x] Started
- [x] Complete

### Tasks Learning
- Creating a Project
- Issues
  - create
  - create labels
  - add labels
  - quick actions
  - moving an issue
  - closing an issue
- Epics
  - What is an Epic
  - Identify Epics
  - how to use epics
    - Updating
    - Labels
    - Importing
    - Linking Epics and Issues
    - Issue Health Status
    - Promote issue to epic
    - [Epics vs. Issues](https://cloud.scorm.com/content/courses/L4NF4NVD5S/gitlab8570041/8/assets/T5BWwnibOZT3qgBk_mREWIBYsu-mWIOrg-Issues%20vs%20epics.pdf)
- Merge Requests
  - [Cheat Sheet](https://cloud.scorm.com/content/courses/L4NF4NVD5S/gitlab8570041/8/assets/aoLsA2TFCl1FgGV-_F059yEZlpl78JiTd-Merge%20requests.pdf)
  

## A little bit of task management
- [x] Started
- [ ] Completed

### Milestones
- [Cheat Sheat vs. Iterations](https://cloud.scorm.com/content/courses/L4NF4NVD5S/gitlab8570041/8/assets/Eue1JNMAaerMtZC__2P63ZUITG0uepq04-Milestones%20vs%20iterations.pdf)


## Creating your own project
- [ ] Started
- [ ] Completed


## Getting ready for Handbook first
- [ ] Started
- [ ] Completed


## Oh, no, Gitlab Seems different today
- [ ] Started
- [ ] Completed


## Final Check
- [ ] Started
- [ ] Completed
